const express = require('express')
const app = express()
const pg = require('pg');

models = require('./models');

// Param BDD
const Sequelize = require('sequelize');
const sequelize = new Sequelize('bobyMovie', 'postgres', 'postgres', {
  host: 'localhost',
  dialect: 'postgres',
  operatorsAliases: false,

  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  },
});

// connect to BDD
sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  });

// Var for send data
var movies = [];
var presses = [];
var users = [];
  
// search Movie
sequelize.query('SELECT * FROM movie', { model: models.Movie }).then(movie => {
  movies = movie;
})

sequelize.query('SELECT * FROM press', { model: models.Press }).then(press => {
  presses = press;
})

sequelize.query('SELECT * FROM users', { model: models.User }).then(user => {
  users = user;
})

// accept all front-end
app.all('/*', function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With");
  next();
});

// get Movies
app.get('/movie', function (req, res) {
  res.send(movies)
})

app.get('/press', function (req, res) {
  res.send(presses)
})

app.get('/user', function (req, res) {
  res.send(users)
})

// Start Server
app.listen(3000, function () {
  console.log('Example app listening on port 3000!')
})