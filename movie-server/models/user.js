'use strict';
module.exports = (sequelize, DataTypes) => {
  var User = sequelize.define('User', {
    codeUser: DataTypes.INTEGER,
    nameUser: DataTypes.STRING,
    passwordUser: DataTypes.STRING,
    mailUser: DataTypes.STRING,
    descriptionUser: DataTypes.STRING,
    codePressUser: DataTypes.STRING
  }, {});
  User.associate = function(models) {
    // associations can be defined here
  };
  return User;
};