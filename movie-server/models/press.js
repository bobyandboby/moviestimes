'use strict';
module.exports = (sequelize, DataTypes) => {
  var Press = sequelize.define('Press', {
    codePress: DataTypes.STRING,
    namePress: DataTypes.STRING
  }, {});
  Press.associate = function(models) {
    // associations can be defined here
  };
  return Press;
};