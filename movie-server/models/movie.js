'use strict';
module.exports = (sequelize, DataTypes) => {
  var Movie = sequelize.define('Movie', {
    codeMovie: DataTypes.STRING,
    nameMovie: DataTypes.STRING,
    comingSoon: DataTypes.DATE,
    descriptionMovie: DataTypes.STRING,
    codeSagaMovie: DataTypes.STRING
  }, {});
  return Movie;
};