--
-- PostgreSQL database dump
--

-- Dumped from database version 10.4
-- Dumped by pg_dump version 10.4

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: belong; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.belong (
    "codeTypeTechnicalBelong" character varying(10) NOT NULL,
    "codeTechnicalBelong" bigint NOT NULL
);


ALTER TABLE public.belong OWNER TO postgres;

--
-- Name: criticalSaga; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."criticalSaga" (
    "codeUser" bigint NOT NULL,
    "codeSaga" character varying(10) NOT NULL,
    description character varying(250) NOT NULL
);


ALTER TABLE public."criticalSaga" OWNER TO postgres;

--
-- Name: criticalTechnical; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."criticalTechnical" (
    "codeUser" bigint NOT NULL,
    "codeTechnical" bigint NOT NULL,
    "descriptionCriticalTechnical" character varying(250)
);


ALTER TABLE public."criticalTechnical" OWNER TO postgres;

--
-- Name: movie; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.movie (
    "codeMovie" character varying(10) NOT NULL,
    "nameMovie" character varying(50) NOT NULL,
    "comingSoon" date,
    "descriptionMovie" character varying(250) NOT NULL,
    "codeSagaMovie" character varying
);


ALTER TABLE public.movie OWNER TO postgres;

--
-- Name: press; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.press (
    "codePress" character varying(10) NOT NULL,
    "namePress" character varying(50)
);


ALTER TABLE public.press OWNER TO postgres;

--
-- Name: saga; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.saga (
    "codeSaga" character varying(10) NOT NULL,
    "nameSaga" character varying(50) NOT NULL,
    "descriptionSaga" character varying(250),
    "dateBeginSaga" date,
    "dateEndSaga" date
);


ALTER TABLE public.saga OWNER TO postgres;

--
-- Name: technical; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.technical (
    "codeTechnical" bigint NOT NULL,
    "nameTechnical" character varying(50) NOT NULL,
    "descriptionTechnical" character varying(250),
    "dateBirthdayTechnical" date NOT NULL,
    "dateDeathTechnical" date
);


ALTER TABLE public.technical OWNER TO postgres;

--
-- Name: toPlay; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."toPlay" (
    "codeTechnical" bigint NOT NULL,
    "codeMovie" character varying NOT NULL
);


ALTER TABLE public."toPlay" OWNER TO postgres;

--
-- Name: toWatch; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."toWatch" (
    "codeUser" bigint NOT NULL,
    "codeMovie" character varying(10) NOT NULL,
    "criticalToWatch" character varying(250),
    "viewToWatch" boolean
);


ALTER TABLE public."toWatch" OWNER TO postgres;

--
-- Name: typeTechnical; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."typeTechnical" (
    "codeTypeTechnical" character varying(10) NOT NULL,
    "nameTypeTechnical" character varying(50) NOT NULL
);


ALTER TABLE public."typeTechnical" OWNER TO postgres;

--
-- Name: user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."user" (
    "codeUser" bigint NOT NULL,
    "nameUser" character varying(50) NOT NULL,
    "passwordUser" character varying(50),
    "mailUser" character varying(50),
    "descriptionUser" character varying(50),
    "codePressUser" character varying(10)
);


ALTER TABLE public."user" OWNER TO postgres;

--
-- Data for Name: belong; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.belong ("codeTypeTechnicalBelong", "codeTechnicalBelong") FROM stdin;
act	1
\.


--
-- Data for Name: criticalSaga; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."criticalSaga" ("codeUser", "codeSaga", description) FROM stdin;
1	starWars	Bonne saga, les derniers episode ne sont pas terrible
\.


--
-- Data for Name: criticalTechnical; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."criticalTechnical" ("codeUser", "codeTechnical", "descriptionCriticalTechnical") FROM stdin;
1	1	Je pense que cet acteur est genial
4	2	Tres mechant comme mec dans harry potter
10	2	Bonne acteur dans harry potter
18	2	Bon acteur dans HP
18	3	Bon acteur dans HP
18	4	Bon acteur dans HP
20	4	Je le kiff trop
16	3	Bon acteur
14	2	j aimerais bien connaitre les autres films dan lesquelle il a joué
\.


--
-- Data for Name: movie; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.movie ("codeMovie", "nameMovie", "comingSoon", "descriptionMovie", "codeSagaMovie") FROM stdin;
SWEP4	Star Wars, épisode IV : Un nouvel espoir	1977-01-01	C est le premier opus de la saga Star Wars par sa date de sortie	starWars
inglorBast	Inglourious Basterds	2009-01-01	Inglourious Basterds "le commando des batards"	\N
oss117lcne	OSS 117: Le caire nid d espions	2006-01-01	Film jean dujardin au caire	oss117
oss117rnrp	OSS 117: Rio ne repond plus	2009-01-01	Film jean dujardin à Rio	oss117
ironman1	Iron Man	2008-01-01	Les dzbuts d IronMan	ironman
ironman3	Iron Man	2013-01-01	Tony Stark prends la confiance	ironman
harrypott1	Harry Potter et la chambres des secrets	2002-01-01	Harry potter et la chambre des secrets	harryPot
harrypott3	Harry Potter et le prisonnier d Azkaban	2004-01-01	Harry potter devien un taulards	harryPot
\.


--
-- Data for Name: press; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.press ("codePress", "namePress") FROM stdin;
leMondeFr	Journal le monde
leparisien	Le parisien
allocine	Allocine
tvmagazine	TV Magazine
versfemina	Version femina
tele7jours	Tele 7 jours
telez	Tele Z
telestar	Télé star
teleloisir	Télé loisirs
feactuel	Femme Actuel
parismatch	Paris Match
telerama	Télérama
mmefigaro	Madame Figaro
marieclair	Marie Claire
lepoint	Le Point
lequipe	L équipe
express	L Éxpress
le20minute	20 Minutes
closer	Closer
voici	Voici
\.


--
-- Data for Name: saga; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.saga ("codeSaga", "nameSaga", "descriptionSaga", "dateBeginSaga", "dateEndSaga") FROM stdin;
starWars	Star Wars	Star Wars un univers	2019-01-01	2019-01-01
harryPot	Harry Potter	Harry potter, un jeune sorcier	2001-01-01	2011-01-01
oss117	OSS 117	oss117 un agent secret francais	1957-01-01	2009-01-01
alien	Alien	Invasion d alien	\N	\N
americanpi	American Pie	Des adolescent font la fête	\N	\N
austinpowe	Austin Powers	Austin Powers un agent secret spéciale	1997-01-01	\N
batman	Batman	Un super héros	\N	\N
destfinale	Destination Finale	La mort rattrappe tout le mondse	\N	\N
diehard	Die Hard	Die Hard, connais pas	\N	\N
fastandfu	Fast and Furious	Des voitures, des meufs on a tout	2001-01-01	2017-01-01
ironman	Iron Man	Un homme de fer	2008-01-01	2013-01-01
\.


--
-- Data for Name: technical; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.technical ("codeTechnical", "nameTechnical", "descriptionTechnical", "dateBirthdayTechnical", "dateDeathTechnical") FROM stdin;
1	Jean dujardin	jean dujardin est moche	1977-07-09	\N
2	Tom Felton	acteur dans harry potter	1987-09-22	\N
3	Matthew Lewis	acteur harry potter	1989-06-27	\N
4	Richard Harris	acteur hp	1930-10-01	\N
\.


--
-- Data for Name: toPlay; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."toPlay" ("codeTechnical", "codeMovie") FROM stdin;
1	oss117rnrp
1	oss117lcne
2	harrypott3
3	harrypott3
4	harrypott3
\.


--
-- Data for Name: toWatch; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."toWatch" ("codeUser", "codeMovie", "criticalToWatch", "viewToWatch") FROM stdin;
1	SWEP4	Tres bon film	t
4	harrypott3	Tres bon film vivement le prochain !	t
10	harrypott3	Encore pas deçus	t
9	harrypott3	Genial	t
\.


--
-- Data for Name: typeTechnical; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."typeTechnical" ("codeTypeTechnical", "nameTypeTechnical") FROM stdin;
act	acteur
prod	producteur
\.


--
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."user" ("codeUser", "nameUser", "passwordUser", "mailUser", "descriptionUser", "codePressUser") FROM stdin;
3	Jules	jules	jules.agostini@epsi.fr	j aime les nanar	\N
4	Romain	Romain	romain.vaidie@epsi.fr	vive harry potter	\N
1	nameUser	password	password@gmail.com	je suis nouveau je ne sais quoi dire	\N
2	drogo le gitan	password	password@gmail.com	je suis nouveau je ne sais quoi dire	\N
5	admin	admin	admin@gmail.com	je suis un Admin fais attention	\N
6	utilisateurs allocine	allocine	allocine@gmail.com	je represente allociné	allocine
7	utilisateurs closer	closer	closer@gmail.com	je represente closer	closer
8	utilisateurs express	express	express@gmail.com	je represente express	express
9	utilisateurs teleloisir	teleloisir	teleloisir@gmail.com	je represente teleloisir	teleloisir
10	utilisateurs marieclair	marieclair	marieclair@gmail.com	je represente marieclair	marieclair
11	Aubin Brosset	aubinbrosset	aubinbrosset@gmail.com	je suis aubin brosset	\N
12	Théo Savin	theosavin	yheosavin@gmail.com	je suis Théo Savin	\N
13	Toto	toto	toto@gmail.com	je suis toto	\N
14	Tata	tata	tata@gmail.com	je suis tata	\N
15	Louis Mouelo	louis	louis@gmail.com	je suis louis	\N
16	Alan Guegan	alan	alan@gmail.com	je suis alan	\N
17	Naruto Uzumaki	Naruto	naruto@gmail.com	je suis naruto	\N
18	Sasuke Uchiwa	sasuke	sasuke@gmail.com	je suis sasuke	\N
19	Sacha bourpalette	sachatte	sacha@gmail.com	je suis sacha	\N
20	test	test	test@gmail.com	je suis test	\N
21	Gaara	Gaara	Gaara@gmail.com	je suis Gaara	\N
\.


--
-- Name: belong belong_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.belong
    ADD CONSTRAINT belong_pkey PRIMARY KEY ("codeTypeTechnicalBelong", "codeTechnicalBelong");


--
-- Name: criticalSaga criticalSaga_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."criticalSaga"
    ADD CONSTRAINT "criticalSaga_pkey" PRIMARY KEY ("codeUser", "codeSaga");


--
-- Name: criticalTechnical criticalTechnical_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."criticalTechnical"
    ADD CONSTRAINT "criticalTechnical_pkey" PRIMARY KEY ("codeUser", "codeTechnical");


--
-- Name: movie movie_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.movie
    ADD CONSTRAINT movie_pkey PRIMARY KEY ("codeMovie");


--
-- Name: press press_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.press
    ADD CONSTRAINT press_pkey PRIMARY KEY ("codePress");


--
-- Name: saga saga_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.saga
    ADD CONSTRAINT saga_pkey PRIMARY KEY ("codeSaga");


--
-- Name: technical technical_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.technical
    ADD CONSTRAINT technical_pkey PRIMARY KEY ("codeTechnical");


--
-- Name: typeTechnical typeTechnical_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."typeTechnical"
    ADD CONSTRAINT "typeTechnical_pkey" PRIMARY KEY ("codeTypeTechnical");


--
-- Name: user user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_pkey PRIMARY KEY ("codeUser");


--
-- Name: toWatch codeMovie; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."toWatch"
    ADD CONSTRAINT "codeMovie" FOREIGN KEY ("codeMovie") REFERENCES public.movie("codeMovie");


--
-- Name: toPlay codeMovie; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."toPlay"
    ADD CONSTRAINT "codeMovie" FOREIGN KEY ("codeMovie") REFERENCES public.movie("codeMovie");


--
-- Name: user codePressUser; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT "codePressUser" FOREIGN KEY ("codePressUser") REFERENCES public.press("codePress");


--
-- Name: criticalSaga codeSaga; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."criticalSaga"
    ADD CONSTRAINT "codeSaga" FOREIGN KEY ("codeSaga") REFERENCES public.saga("codeSaga");


--
-- Name: movie codeSagaMovie; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.movie
    ADD CONSTRAINT "codeSagaMovie" FOREIGN KEY ("codeSagaMovie") REFERENCES public.saga("codeSaga");


--
-- Name: criticalTechnical codeTechnical; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."criticalTechnical"
    ADD CONSTRAINT "codeTechnical" FOREIGN KEY ("codeTechnical") REFERENCES public.technical("codeTechnical");


--
-- Name: toPlay codeTechnical; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."toPlay"
    ADD CONSTRAINT "codeTechnical" FOREIGN KEY ("codeTechnical") REFERENCES public.technical("codeTechnical");


--
-- Name: belong codeTechnicalBelong; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.belong
    ADD CONSTRAINT "codeTechnicalBelong" FOREIGN KEY ("codeTechnicalBelong") REFERENCES public.technical("codeTechnical");


--
-- Name: belong codeTypeTechnicalBelong; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.belong
    ADD CONSTRAINT "codeTypeTechnicalBelong" FOREIGN KEY ("codeTypeTechnicalBelong") REFERENCES public."typeTechnical"("codeTypeTechnical");


--
-- Name: toWatch codeUser; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."toWatch"
    ADD CONSTRAINT "codeUser" FOREIGN KEY ("codeUser") REFERENCES public."user"("codeUser");


--
-- Name: criticalTechnical codeUser; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."criticalTechnical"
    ADD CONSTRAINT "codeUser" FOREIGN KEY ("codeUser") REFERENCES public."user"("codeUser");


--
-- Name: criticalSaga codeUser; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."criticalSaga"
    ADD CONSTRAINT "codeUser" FOREIGN KEY ("codeUser") REFERENCES public."user"("codeUser");


--
-- PostgreSQL database dump complete
--

