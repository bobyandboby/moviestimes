import { Component, OnInit } from '@angular/core';
import { MovieService, Movie } from '../../service/movie.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})

export class HomePageComponent implements OnInit {
  
  movieList: Movie[];
  test = [];
  constructor(private movieService : MovieService) { }

  ngOnInit() {
    
    // console.log(this.movieService.getFilm());
    this.movieService.getFilm().subscribe((data) =>   {
      this.movieList = data;
    });
    
  }

  get(data) {
    this.test = data;
    return this.test;
  }

}
