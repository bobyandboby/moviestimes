import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs';
import * as lodash from 'lodash';

export interface Movie {
  codeMovie: string;
  nameMovie: string;
  comingSoon: string;
  descriptionMovie: string;
  codeSagaMovie?: string;
}

@Injectable()
export class MovieService {

  constructor(private readonly _http: HttpClient) { }

  getFilm(): Observable<Movie[]> {
    return this._http.get<Movie[]>('http://localhost:3000/movie');
  }
}